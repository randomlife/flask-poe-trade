import sys
import trade

from trade import config
from trade.app import App
from trade.command import Command

flask_config = config.config['flask']
user_config = config.config['user']

app = App(__name__)
app.config.update(flask_config)
app.register_blueprint(trade.blueprint.bp)

if __name__ == '__main__':
    if len(sys.argv) >= 2:
        for command in sys.argv[1:]:
            Command.invoke(command)
        sys.exit(0)
    else:
        app.run()
