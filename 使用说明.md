## V1.0.3使用说明

[TOC]



### 启动个人市集服务

双击打开 **server.exe**

![Snipaste_2020-12-01_18-18-52](docs/images/Snipaste_2020-12-01_18-18-52.png)

出现如下文字信息时, 即代表启动成功

![Snipaste_2020-12-01_18-21-55](docs/images/Snipaste_2020-12-01_18-21-55.png)



### 访问个人市集

打开浏览器, 地址栏输入`127.0.0.1:5000` 或 `localhost:5000`  ,  即可正常使用;

![Snipaste_2020-12-01_18-24-16](docs/images/Snipaste_2020-12-01_18-24-16.png)



### 如何使用实时搜索

(仅提供 谷歌浏览器,  最新版edge浏览器(chrome内核) 的操作说明)

1. 在腾讯的官方市集上登录个人账号, 如图保证当前处于登录状态

   ![Snipaste_2020-12-01_18-30-21](docs/images/Snipaste_2020-12-01_18-30-21.png)

2. 获取当前登录状态的cookie数据

   右键图中`小锁`

   单击下面的`（使用了 XX 个）Cookie`

   ![Snipaste_2020-12-01_22-46-05](docs/images/Snipaste_2020-12-01_22-46-05.png)

   在弹出的窗口中, 依次找到 `poe.game.qq.com` -> `Cookie` -> `POESESSID`

   ![Snipaste_2020-12-01_22-50-16](docs/images/Snipaste_2020-12-01_22-50-16.png)

   并将所示的 `名称`和`内容` 组合为如下格式

   ```
   POESESSID=8a17753a79040bb71e837978xxxxx
   ```

3. 拷贝到 `server.exe` 所在目录下的`cookie.txt`文件中, 保存并关闭; 不要自己加多余的空格或换行符;

   ![Snipaste_2020-12-01_22-58-03](docs/images/Snipaste_2020-12-01_22-58-03.png)

4. 在本地的个人市集网页中单击`激活实时搜索`

   ![Snipaste_2020-12-01_22-59-27](docs/images/Snipaste_2020-12-01_22-59-27.png)

5. 如果正常的话, `server.exe` 程序会输出类似的日志信息

   ```
   [2020-12-01 23:02:12,363] INFO in ws_server: WS服务(2048167160968): 启动
   [2020-12-01 23:02:12,580] INFO in ws_client: WS客户端(2048165197448): 启动
   [2020-12-01 23:02:14,126] INFO in ws_client: WS客户端(2048165197448): 收到推送, 2条
   ```

6. 设置cookie的操作一般过一段时间可以需要操作一次， 不是每次重启服务都需要操作,  因为cookie(认证信息)本身是有有效时间的，这个有效时间一般是至少有四五天吧， 这个要看腾讯官方的策略；

### 实时搜索常见问题

#### 1) cookie过期

当我们`激活实时搜索后`

`server.exe` 程序输出如下的日志(认证失败, 请检查cookie是否过期 )

```
[2020-12-01 23:11:38,909] ERROR in ws_server: WS服务(2048165760520)->WS-客户端: 异常退出, 认证失败, 请检查cookie是否过期 ,1秒后自动重启
[2020-12-01 23:11:40,062] ERROR in ws_server: WS服务(2048165760520)->WS-客户端: 异常退出, 认证失败, 请检查cookie是否过期 ,1秒后自动重启
[2020-12-01 23:11:41,212] ERROR in ws_server: WS服务(2048165760520)->WS-客户端: 异常退出, 认证失败, 请检查cookie是否过期 ,1秒后自动重启
[2020-12-01 23:11:42,375] ERROR in ws_server: WS服务(2048165760520)->WS-客户端: 异常退出, 认证失败, 请检查cookie是否过期
```

代表cookie(用户认证信息)失效,  需要去poe官网重新登录，并拷贝最新的cookie至`cookie.txt`文件中

#### 2) 未配置cookie

当我们`激活实时搜索后`

`server.exe` 程序输出如下的日志(cookie文件未正确配置, 无法使用实时搜索 )

```
[2020-12-01 23:15:56,762] ERROR in ws_server: WS服务(2048167405832)->WS-客户端: 异常退出, cookie文件未正确配置, 无法使用实时搜索 ,1秒后自动重启
[2020-12-01 23:15:57,764] ERROR in ws_server: WS服务(2048167405832)->WS-客户端: 异常退出, cookie文件未正确配置, 无法使用实时搜索 ,1秒后自动重启
[2020-12-01 23:15:58,764] ERROR in ws_server: WS服务(2048167405832)->WS-客户端: 异常退出, cookie文件未正确配置, 无法使用实时搜索 ,1秒后自动重启
```

代表当前目录下没有`cookie.txt`文件, 或者该文件是空白

如果希望使用实时搜索 ，需要去poe官网重新登录，并拷贝最新的cookie至`cookie.txt`文件中



### 脚本说明

#### 1)【资源文件】备份.bat

- 双击运行
- 用途: 将当前的网页资源文件(默认: static文件夹)备份至压缩文件中, 防止之后执行不可预期的操作导致资源文件损害,无法打开本地市集页, 可以通过备份文件恢复数据

#### 2)【资源文件】清理.bat

- 双击运行
- 用途: 清空当前网页资源文件夹(默认: static文件夹)中的数据

#### 3)【资源文件】远程更新(恢复).bat

- 双击运行

- 较重要的脚本

- 用途:  

  1. 本地的网页资源文件(默认: static文件夹)损坏时, 可以通过此脚本从本项目的线上地址(git)恢复网页资源文件; 我为大家维护好了近期腾讯官方关键的静态资源文件, 不定期更新；

  2. 本地的网页资源文件(默认: static文件夹)比较旧， 并且与腾讯官方的功能有明显差异的时候, 通过此脚本可以从本项目的线上地址(git)更新网页资源文件为一个较新的版本;

  3. 何时执行本脚本?

     每次启动服务的程序的时候，软件会自动将本地的资源文件与远程的资源文件进行版本对比, 如果有更新的版本, 会有文字提示

     ![Snipaste_2020-12-01_23-43-48](docs/images/Snipaste_2020-12-01_23-43-48.png)

     你可以按照提示更新， 或者在使用中发现本地市集的功能与官方有明显差别时再去执行更新脚本

  4. 更新脚本后应当清空浏览器缓存, 或者在设置中点击清空缓存按钮, 后手动刷新页面

     ![Snipaste_2020-12-01_23-48-36](docs/images/Snipaste_2020-12-01_23-48-36.png)



### 更高级的资源更新操作

**此操作一定要在poe官方市集可以正常打开，没有挂掉, 一般上午时分用的人少, 官方市集都是可以正常访问的**

因为本程序会自动将本地没有的资源文件从官网拉取最新的文件并保存下来, 依赖此特性, 你可以不用执行`【资源文件】远程更新(恢复).bat`这样的脚本来更新官方最新的文件

仅需要将本机的资源文件(默认:static文件夹)夹删除掉后， 然后重新刷新页面(127.0.0.1:5000)即可， 有可能文件下载失败, 多刷新几次

![Snipaste_2020-12-01_23-58-06](docs/images/Snipaste_2020-12-01_23-58-06.png)

刷新页面后， 可以看到日志:

![Snipaste_2020-12-02_00-00-19](docs/images/Snipaste_2020-12-02_00-00-19.png)

说明程序存储了最新的资源文件

当然通过这种手动更新的资源文件， 系统是没有为其分配版本号, 所以每次启动程序的时候会提示本地未知版本，并要求你更新

可以不用理会这个提示,  以后完全自己通过上述操作来更新官方最新的文件

```
[2020-12-01 23:59:43,712] INFO in app: [资源文件信息]远程(git): 2020-12-01(7d12b7dd-287f-40cd-bdbc-1b749a78333e)
[2020-12-01 23:59:43,713] INFO in app: [资源文件信息]本地(loc): 未知版本
[2020-12-01 23:59:43,713] INFO in app: 资源文件有可用更新,  可以手动执行更新脚本!!!!!!!
[2020-12-01 23:59:43,713] INFO in app: 资源文件有可用更新,  可以手动执行更新脚本!!!!!!!
[2020-12-01 23:59:43,713] INFO in app: 资源文件有可用更新,  可以手动执行更新脚本!!!!!!!
```



最后一定不能忘了要在设置中点击`清空缓存`按钮， 并刷新网页