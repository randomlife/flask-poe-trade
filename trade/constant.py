# -*- coding: utf-8 -*-
# @file    : constant
# @Date    : 2020/11/19
# @Author  :
# @Version : 1.0.0
import os
import sys

# 调试模式
DEBUG = False
# app 根目录
ROOT_PATH = os.path.dirname(sys.argv[0])
# 市集相关地址
POE_HOST = "poe.game.qq.com"
POE_REMOTE = "https://" + POE_HOST
POE_INDEX = POE_REMOTE + '/trade'
CDN_HOST = 'poecdn.game.qq.com'
CDN_REMOTE = "https://" + CDN_HOST
# 配置文件的默认路径
CONFIG_PATH = './config.ini'
# User Agent
DEFAULT_UA = "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36"
# 项目地址
GIT = "https://gitee.com/randomlife/flask-poe-trade"
# 数据备份路径
STATIC_BAK_PATH = "./backup"
# 日期时间默认格式
DTF = "%Y%m%d%H%M%S"
