# -*- coding: utf-8 -*-
# @file    : logger
# @Date    : 2020/11/19
# @Author  :
# @Version : 1.0.0
from logging.config import dictConfig
from logging import getLogger

# 日志配置
dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

logger = getLogger()
