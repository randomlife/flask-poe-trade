# -*- coding: utf-8 -*-
# @file    : _covert_func
# @Date    : 2020/11/26
# @Author  :
# @Version : 1.0.0
"""
Covert Functions for Config Option
"""


def api_cache_list(value: str):
    """
    缓存接口列表 转换函数
    :param value:
    :return:
    """
    covert = []
    parts = value.split(",")
    for part in parts:
        covert.append(part.strip())

    return covert
